#!/bin/bash

set -eu

data_dir="/app/data"
SSL_KEY=/app/data/server.key
SSL_CERT=/app/data/server.crt
SSL_FINGERPRINT=/app/data/server.crt.fingerprint

if [[ -z "$(ls -A "${data_dir}" >/dev/null)" ]]; then
    echo "Empty data directory, copying configs"

    # atleast, motd needs to be backed up
    cp /app/configs/ngircd/* "${data_dir}"

    openssl req -x509 -nodes -days 365 -newkey rsa:4096 -keyout "${SSL_KEY}" -out "${SSL_CERT}" -batch
    fingerprint=`cat "${SSL_CERT}" | openssl x509 -fingerprint | head -1 | sed "s/SHA1 Fingerprint=//"`
    echo "${fingerprint}" > ${SSL_FINGERPRINT}
    echo "Server cert fingerprint: ${fingerprint}"
fi

/usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i NGIRCD
