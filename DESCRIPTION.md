Internet Relay Chat server and Web UI for small or private networks

## Why use this app?

  * Uses ngIRCd is being actively developed since 14 years
  * Simple
  * Supports SSL
  * Integrated with Shout Web UI

## Mode documentation

  [Modes](http://ngircd.barton.de/doc/Modes.txt)

## Password protected channels
  * `/mode #channel +k trustno1` and then join with `/JOIN #channel trustno1`

## Operator commands

  * `/kick <nick> <reason>` to kick someone
  * /mode <channel> +o <nick> to give op status to another person
