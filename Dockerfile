FROM cloudron/base:0.10.0
MAINTAINER Girish Ramakrishnan <girish@cloudron.io>

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get -y install ngircd && rm -r /var/cache/apt /var/lib/apt/lists

ENV PATH /usr/local/node-6.9.5/bin:$PATH

ADD config /app/configs/ngircd
ADD settings /app/code/settings

ADD supervisor/ /etc/supervisor/conf.d/

RUN rm /etc/nginx/sites-enabled/*
ADD nginx-ircd /etc/nginx/sites-enabled/ircd-reverse-proxy

RUN mkdir -p /app/code
ADD start.sh /app/code/start.sh
RUN chmod +x /app/code/start.sh

WORKDIR /app/code/settings
RUN npm install --production

RUN npm install -g shout
ADD config/shout-config.js /home/cloudron/.shout/config.js
RUN mkdir /home/cloudron/.shout/users # shout really wants this directory

# expose nginx port
EXPOSE 8080

CMD [ "/app/code/start.sh" ]

