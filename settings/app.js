#!/usr/bin/env node

/* jslint node:true */

'use strict';

require('supererror');

var express = require('express'),
    ejs = require('ejs'),
    path = require('path'),
    debug = require('debug')('server'),
    superagent = require('superagent'),
    session = require('express-session'),
    logger = require('morgan'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    ngircd = require('./ngircd.js'),
    os = require('os');

// accept self signed certs
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';


var port = 7777;
var appData = process.env.APP_DATA || '/app/data';
var clientId = process.env.OAUTH_CLIENT_ID || 'cid-test';
var clientSecret = process.env.OAUTH_CLIENT_SECRET || 'unused';
var appOrigin = process.env.CLOUDRON ? 'https://' + os.hostname() + '/admin' : 'http://localhost:7777';
var callbackURL = appOrigin + '/oauth_callback';
var adminOrigin = process.env.API_ORIGIN || 'https://admin-localhost';
var oAuthBeginURL = adminOrigin + '/api/v1/oauth/dialog/authorize?response_type=code&client_id=' + clientId + '&redirect_uri=' + callbackURL + '&scope=profile';


console.log('');
console.log('==================================');
console.log(' Configuration                    ');
console.log('==================================');
console.log('Port:                ', port);
console.log('Base dir:            ', appData);
console.log('OAuth client id      ', clientId);
console.log('OAuth client secret: ', clientSecret);
console.log('OAuth callback url:  ', callbackURL);
console.log('App origin:          ', appOrigin);
console.log('Admin origin:        ', adminOrigin);
console.log('==================================');
console.log('');


function indexPage(req, res) {
    if (!req.session || !req.session.token || !req.session.user) return res.redirect(appOrigin + '/perform_oauth');

    ngircd.getViewContext('index.html', function (error, result) {
        if (error) res.send(500, error);
        res.render('index.html', result);
    });
}

function healthcheck(req, res) {
    res.send(200, {});
}

function oAuthBegin(req, res) {
    res.redirect(oAuthBeginURL);
}

function oAuthEnd(req, res) {
    if (!req.query || !req.query.code) return res.send(550, 'No code found.');

    debug('oAuthEnd: Got code as result of OAuth flow.', req.query.code);

    var data = {
        grant_type: 'authorization_code',
        code: req.query.code,
        redirect_uri: appOrigin,
        client_id: clientId,
        client_secret: clientSecret
    };
    var query = {
        response_type: 'token',
        client_id: data.client_id
    };

    superagent.post(adminOrigin + '/api/v1/oauth/token').query(query).send(data).end(function (error, result) {
        if (error) return res.send(500, error);
        if (result.status !== 200) return res.send(401, 'Not authorized');

        debug('oAuthEnd: Got access token', result.body.access_token);

        req.session.token = result.body.access_token;

        superagent.get(adminOrigin + '/api/v1/profile').query({ access_token: req.session.token }).end(function (error, result) {
            if (error) return res.send(500, error);
            if (result.status !== 200) return res.send(401, 'Not authorized');

            debug('oAuthEnd: Got user profile', result.body);
            req.session.user = result.body;

            res.redirect(appOrigin);
        });
    });
}

function logout(req, res) {
    var token = req.session ? req.session.token : null;

    if (req.session) {
        debug('logout: destroying session.');
        req.session.destroy();
    }

    if (!token) {
        debug('logout: success.');
        res.redirect(appOrigin);
        return;
    }

    res.redirect(adminOrigin + '/api/v1/session/logout?redirect=' + appOrigin);
}

// setup router
var router = express.Router();

// main views
router.get('/', indexPage);

// OAuth and session
router.get('/perform_oauth', oAuthBegin);
router.get('/oauth_callback', oAuthEnd);
router.get('/logout', logout);

// healthcheck
router.get('/healthcheck', healthcheck);

// app specific routes
router.get('/api/config', ngircd.getConfig);
router.post('/api/config', ngircd.setConfig);

// init user components and start server
// This allows the api to attach custom routes for business logic and additional views
ngircd.init({ appData: appData }, function (error) {
    if (error) {
        console.error('Unable to initialize API', error);
        process.exit(1);
    }

    // setup express app
    var app = express();
    app.set('views', path.join(__dirname, '/views'));
    app.engine('html', ejs.renderFile);

    app.use(logger('dev', { immediate: false }));
    app.use(express.static(path.join(__dirname, '/public')));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded());
    app.use(methodOverride());
    app.use(session({ secret: 'a tree is blue' }));
    app.use(router);

    app.listen(7777, function (error) {
        if (error) {
            console.error('Unable to listen on port %d.', port);
            process.exit(1);
        }

        console.log('Server listening on port %d...', port);
    });
});
