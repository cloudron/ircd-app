/* jslint node:true */

'use strict';

var debug = require('debug')('server:api'),
    ini = require('ini'),
    path = require('path'),
    execFile = require('child_process').execFile,
    url = require('url'),
    fs = require('fs'),
    safe = require('safetydance'),
    os = require('os');

module.exports = exports = {
    init: init,
    getViewContext: getViewContext,
    getConfig: getConfig,
    setConfig: setConfig
};

var gConfig = {};
var gConfigFile = null;
var gMotdFile = null;

function init(options, callback) {

    gConfigFile = path.join(options.appData, 'ngircd.conf');
    gMotdFile = path.join(options.appData, 'ngircd.motd');

    // read current config file
    gConfig = ini.parse(safe.fs.readFileSync(gConfigFile, 'utf8'));

    // sync the latest hostname in config file
    if (gConfig.Global.Name !== os.hostname()) gConfig.Global.Name = os.hostname();
    if (gConfig.Global.Ports !== process.env.NONSSL_IRC_PORT) gConfig.Global.Ports = process.env.NONSSL_IRC_PORT || '';
    if (gConfig.SSL.Ports !== process.env.SSL_IRC_PORT) gConfig.SSL.Ports = process.env.SSL_IRC_PORT || '';

    if (!safe.fs.writeFileSync(gConfigFile, ini.stringify(gConfig))) {
        console.error(safe.error);
    }

    restartNgircd(callback);
}

function getViewContext(view, callback) {
    debug('getViewContext: get for view %s.', view);

    var sslCertFingerprint = fs.readFileSync('/app/data/server.crt.fingerprint', 'utf8');

    var context = {
        title: 'ngIRCd Settings',
        icon: '/ngircd.png',
        fingerprint: sslCertFingerprint
    };

    callback(null, context);
}

function getConfig(req, res) {
    if (!req.session || !req.session.token) return res.send(401);

    res.send(200, {
        adminName: gConfig.Global.AdminInfo1 || '',
        adminEmail: gConfig.Global.AdminEMail || '',
        serverInfo: gConfig.Global.Info || '',
        password: gConfig.Global.Password,
        nonSslPort: process.env.NONSSL_IRC_PORT,
        sslPort: process.env.SSL_IRC_PORT,
        motd: safe.fs.readFileSync(gMotdFile, 'utf8') || ''
    });
}

function restartNgircd(callback) {
    callback = callback || function () { };

    execFile('/usr/bin/supervisorctl', [ 'restart', 'ngircd' ], { timeout: 10000 }, function (error, stdout, stderr) {
        if (error) {
            console.error(error);
            return callback(error);
        }

        debug('setConfig: Restarted ngircd after config change.', stdout, stderr);

        callback(null);
    });
}

function setConfig(req, res) {
    if (!req.body) return res.send(400, 'Body must be JSON');
    if (!req.session || !req.session.token) return res.send(401);

    // ## need to validate against putting random stuff (like newlines) in the fields below
    gConfig.Global.AdminInfo1 = req.body.adminName;
    gConfig.Global.AdminEmail = req.body.adminEmail;
    gConfig.Global.Info = req.body.serverInfo;
    gConfig.Global.Password = req.body.password;

    if (!safe.fs.writeFileSync(gConfigFile, ini.stringify(gConfig))) {
        console.error(safe.error);
        return res.send(500, 'Error saving configuration');
    }

    if (!safe.fs.writeFileSync(gMotdFile, req.body.motd)) {
        console.error(safe.error);
        return res.send(500, 'Error writing motd');
    }

    restartNgircd(function (error) {
        if (error)  return res.send(500, 'Error restarting ngircd');
        res.send(200, { status: 'ok' });
    });
}
