/* jslint browser:true */
/* global $:true */

$(function () {
    'use strict';

    $.get('api/config').done(function (data) {
        $('#inputName').val(data.adminName);
        $('#inputEmail').val(data.adminEmail);
        $('#inputInfo').val(data.serverInfo);
        $('#inputMotd').val(data.motd);
        $('#inputNonSslPort').val(data.nonSslPort);
        $('#inputSslPort').val(data.sslPort);
    }).fail(function (error) {
        console.error('Failed to get config.', error);
    });

    $('#configForm').submit(function (e) {
        var config = {};

        $(this).serializeArray().forEach(function (o) {
            config[o.name] = o.value;
        });

        $.post('api/config', config).done(function () {
            console.log('Update configuration successful.');
        }).fail(function (error) {
            console.error('Failed to update config.', error);
        });

        e.preventDefault();
    });
});
